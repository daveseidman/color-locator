# Color Locator

A two-screen experience, user places their phone up to a screen. The phone determines it's x,y coordinates in screenspace by using the camera on a rainbow pattern on the larger screen. As the phone is localized the rainbow pattern can be shrunk to a small circle around the camera. Using sockets the larger screen can be aware of the phones position and begin displaying shared imagery across both.
