export default class Video {
  constructor(callback) {
    this.callback = callback;
    this.update = this.update.bind(this);
    this.selectCamera = this.selectCamera.bind(this);

    this.element = document.createElement('div');
    this.video = document.createElement('video');
    this.video.setAttribute('muted', '');
    this.video.setAttribute('playsinline', '');

    this.canvas = document.createElement('canvas');

    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerHeight;
    this.context = this.canvas.getContext('2d');
    this.element.className = 'video';
    // this.element.appendChild(this.video);
    this.element.appendChild(this.canvas);
    this.constraints = { audio: false, video: { facingMode: 'environment' } };

    navigator.mediaDevices.getUserMedia(this.constraints).then((stream) => {
      this.video.srcObject = stream;
      const { width, height } = stream.getVideoTracks()[0].getSettings();
      this.canvas.width = width;
      this.canvas.height = height;
      this.video.play().then(() => { this.callback(); });
    });
  }

  update() {
    this.context.drawImage(this.video, 0, 0);
    requestAnimationFrame(this.update);
  }
}
