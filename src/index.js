import './index.scss';
import Video from './modules/Video';

let video;
const isSecondScreen = () => typeof window.orientation !== 'undefined';
const debug = document.createElement('div');
debug.className = 'debug';

const sampleColor = video => video.context.getImageData(320, 240, 1, 1).data.slice(0, -1);


const update = () => {
  debug.innerHTML = sampleColor(video);
  requestAnimationFrame(update);
};

const videoReady = () => {
  // console.log('video ready');
  update();
};

if (isSecondScreen()) {
  video = new Video(videoReady);
  document.body.appendChild(video.element);
  document.body.appendChild(debug);
  debug.innerHTML = 'testing';
} else {
  const background = document.createElement('img');
  background.className = 'background';
  background.src = 'assets/chart2.jpg';
  document.body.appendChild(background);
}
